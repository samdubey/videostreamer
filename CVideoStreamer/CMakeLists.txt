cmake_minimum_required (VERSION 2.8 FATAL_ERROR)
project(CVideoStreamer)
add_library(CVideoStreamer SHARED src/HDataFrame.cpp include/HDataFrame.h src/HVideoStream.cpp include/HVideoStream.h)
set(FFMPEG_INCLUDE "FFMPEG/INCLUDE" CACHE PATH "FFMPEG INCLUDE")
set(VideoStreamer_INCLUDE "VideoStreamer/VideoStreamer" CACHE PATH "VideoStreamer INCLUDE")
set(VideoStreamer_BUILD "VideoStreamer/VideoStreamer/Build" CACHE PATH "VideoStreamer BUILD")
set(FFMPEG_LIB "FFMPEG/LIB" CACHE PATH "FFMPEG LIB")
target_link_libraries(CVideoStreamer ${VideoStreamer_BUILD}/VideoStreamer.lib ${FFMPEG_LIB}/avutil.lib ${FFMPEG_LIB}/avformat.lib ${FFMPEG_LIB}/avcodec.lib ${FFMPEG_LIB}/swscale.lib ${FFMPEG_LIB}/swresample.lib)
include_directories(${FFMPEG_INCLUDE} ${VideoStreamer_INCLUDE} ${CMAKE_SOURCE_DIR})