﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using SFML.Audio;
using NetEXT.TimeFunctions;

namespace VideoStreamerNET
{
    internal class AudioStream : SoundStream
    {
        #region Variables
        private uint _channelcount = 0;
        private Time _audiosamplesrate = Time.Zero;
        private Time _audiosampleposition = Time.Zero;
        private Mutex _timeupdatemutex = null;
        private ConcurrentQueue<AudioFrame> _queuedaudioframes = null;
        private AudioFrame _currentaudioframe = null;
        private bool _isplayingtoeof = false;
        private bool _iseof = false;
        private Time _audiocorrectiontime = Time.Zero;
        private bool _disposed = false;
        #endregion

        #region Properties
        public bool PlayingtoEof
        {
            get
            {
                return _isplayingtoeof;
            }
            set
            {
                _isplayingtoeof = value;
            }
        }
        public bool IsEof
        {
            get
            {
                return _iseof;
            }
            set
            {
                _iseof = value;
            }
        }
        public Time AudioCorrectionTime
        {
            get
            {
                return _audiocorrectiontime;
            }
            set
            {
                _audiocorrectiontime = value;
            }
        }
        #endregion

        #region Contructors/Destructors
        public AudioStream(uint ChannelCount, uint SampleRate, ConcurrentQueue<AudioFrame> CurrentAudioFrameQueue, Time AudioCorrectionTime)
        {
            _audiocorrectiontime = AudioCorrectionTime;
            _channelcount = ChannelCount;
            _audiosamplesrate = Time.FromSeconds(SampleRate);
            _queuedaudioframes = CurrentAudioFrameQueue;
            _timeupdatemutex = new Mutex();
            Initialize(ChannelCount, SampleRate);
        }
        protected override void Destroy(bool disposing)
        {
            if (!_disposed)
            {
                _disposed = true;
                if (_currentaudioframe != null) _currentaudioframe.Dispose();
            }
            base.Destroy(disposing);
        }
        #endregion

        #region Functions
        protected override void OnSeek(TimeSpan timeOffset)
        {
            // not needed
        }
        protected override bool OnGetData(out short[] returnedbuffer)
        {
            while (_queuedaudioframes.Count == 0 && !_isplayingtoeof)
            {
                Thread.Sleep(5);
            }
            if (_queuedaudioframes.Count == 0 && _isplayingtoeof)
            {
                _iseof = true;
                returnedbuffer = new short[0];
                return false;
            }
            _timeupdatemutex.WaitOne();
            if (_currentaudioframe != null) _currentaudioframe.Dispose();
            _currentaudioframe = null;
            bool validbuffer = false;
            short[] finalbuffer = null;
            while (!validbuffer)
            {
                if (_audiosampleposition > AudioCorrectionTime)
                {
                    // skip samples
                    int finalbufferlength = 0;
                    while (finalbufferlength == 0)
                    {
                        if (_currentaudioframe != null) _currentaudioframe.Dispose();
                        _currentaudioframe = null;
                        while (_queuedaudioframes.Count == 0)
                        {
                            _timeupdatemutex.ReleaseMutex();
                            Thread.Sleep(5);
                            if (_queuedaudioframes.Count == 0 && _isplayingtoeof)
                            {
                                _iseof = true;
                                returnedbuffer = new short[0];
                                return false;
                            }
                            _timeupdatemutex.WaitOne();
                        }
                        if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                        {
                            int skipsamplecount = (int)Math.Max((int)Math.Floor((double)_audiosampleposition.Milliseconds * (double)_audiosamplesrate.Seconds), 0) * (int)_channelcount;
                            finalbufferlength = (int)Math.Max(_currentaudioframe.AudioBufferSize - skipsamplecount, 0);
                            _audiosampleposition -= Time.FromSeconds((float)_currentaudioframe.AudioBufferSize / (float)_audiosamplesrate.Seconds / _channelcount);
                            if (finalbufferlength > 0)
                            {
                                short[] audiodata = _currentaudioframe.AudioDataBuffer.ToArray();
                                finalbuffer = new short[finalbufferlength];
                                Array.Copy(audiodata, Math.Min(skipsamplecount, audiodata.Length), finalbuffer, 0, finalbuffer.Length);
                                validbuffer = true;
                            }
                        }
                    }
                }
                else if (_audiosampleposition < -AudioCorrectionTime)
                {
                    // add samples
                    int extrasamplecount = (int)Math.Floor(-_audiosampleposition.Seconds * _audiosamplesrate.Seconds) * (int)_channelcount;
                    _audiosampleposition += Time.FromSeconds(extrasamplecount / _audiosamplesrate.Seconds / _channelcount);
                    if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                    {
                        _audiosampleposition -= Time.FromSeconds(_currentaudioframe.AudioBufferSize / _audiosamplesrate.Seconds / _channelcount);
                        short[] audiodata = _currentaudioframe.AudioDataBuffer.ToArray();
                        finalbuffer = new short[extrasamplecount + audiodata.Length];
                        audiodata.CopyTo(finalbuffer, extrasamplecount);
                        validbuffer = true;
                    }
                    else
                    {
                        _currentaudioframe = null;
                        finalbuffer = new short[extrasamplecount];
                        validbuffer = true;
                    }
                }
                else
                {
                    // continue as normal
                    if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                    {
                        _audiosampleposition -= Time.FromSeconds((float)_currentaudioframe.AudioBufferSize / (float)_audiosamplesrate.Seconds / _channelcount);
                        finalbuffer = _currentaudioframe.AudioDataBuffer.ToArray();
                        validbuffer = true;
                    }
                }
            }
            _timeupdatemutex.ReleaseMutex();
            returnedbuffer = finalbuffer;
            return true;
        }
        public void Update(Time DeltaTime)
        {
            _timeupdatemutex.WaitOne();
            _audiosampleposition += DeltaTime;
            _timeupdatemutex.ReleaseMutex();
        }
        public void ResettoBegining()
        {
            _timeupdatemutex.WaitOne();
            _audiosampleposition = Time.Zero;
            _timeupdatemutex.ReleaseMutex();
        }
        #endregion
    }
}
