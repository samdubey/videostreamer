﻿using System;
using System.Runtime.InteropServices;

namespace VideoStreamerNET
{
    /// <summary>Represents a RGBA array of pixels</summary>
    public class VideoBuffer
    {
        #region Variables
        private IntPtr _handle = IntPtr.Zero;
        #endregion

        #region Properties
        /// <summary>Returns the value of the specified offset in the pixel array</summary>
        public byte this[UInt32 index]
        {
            get
            {
                unsafe
                {
                    byte* basearray = Imports.GetVideoDataBuffer(_handle);
                    return *(basearray + index);
                }
            }
        }
        /// <summary>Returns the length of this pixel array</summary>
        public UInt32 Length
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal VideoBuffer(IntPtr VideoFrameHandle)
        {
            _handle = VideoFrameHandle;
        }
        #endregion

        #region Functions
        /// <summary>Converts the unmanaged RGBA pixel array to a managed RGBA pixel array</summary>
        public byte[] ToArray()
        {
            byte[] managedbytearray = new byte[Length];
            unsafe
            {
                Marshal.Copy(new IntPtr(Imports.GetVideoDataBuffer(_handle)), managedbytearray, 0, (int)Length);
            }
            return managedbytearray;
        }
        #endregion
    }
}
